const state = {
    competencias: {
        solucionDeProblemas: {
            intro: 'Para que en el nivel 3 se sienten las bases para que el estudiante, a lo largo de su trayectoria en educación superior, es recomendable que los docentes se enfoquen en desarrollar y evaluar: ',
                caracteristicas: [{
                        showDescription: false,
                        icon: 'compress-arrows-alt',
                        title: 'Capacidad de síntesis',
                        description: 'Si bien en el nivel ya se trabajó con los estudiantes del nivel formativo 1 el pensamiento analítico, a partir del nivel 2 se deberá ponderar más la capacidad de síntesis de todos los elementos analizados en dos o tres soluciones prácticas a los problemas. Las habilidades asociadas serían la capacidad de reestructurar los elementos analizados para proponer soluciones claras y efectivas.'
                    },
                    {
                        showDescription: false,
                        icon: 'users',
                        title: 'Trabajo en equipo',
                        description: 'A partir de este nivel también se recomienda enseñar a los estudiantes a resolver problemáticas en equipo para lo cual se deberá coadyuvar a los estudiantes en la integración, planeación, organización y gestión de equipos y tareas. También se les debe enseñar técnicas de manejo de conflictos y negociación para que ellos solos vayan gestionando el desempeño de los equipos, mantener la dirección, hasta la consecución de las metas preestablecidas.'
                    }
                ]
        },
        creatividad: {
            intro: 'A partir de este nivel, los estudiantes deberán empezar a tomar decisiones por lo tanto se recomienda desarrollar:',
            caracteristicas: [
                {
                    showDescription: false,
                    title: 'Autonomía',
                    description: 'Para lograr que los estudiantes puedan seguir sus propios criterios y seguir sus instintos se deberá fomentar en ellos la independencia de actuación y su emancipación para que paulatinamente vayan motivándose a actuar por su cuenta y serán capaces de mostrar resultados efectivos sin la tutela de los docentes.',
                    icon: 'address-card'
                }
            ]
        },
        innovacion: {
            intro: 'A partir de este nivel, se les deberá pedir a los estudiantes empezar a implementar las soluciones alternativas ya trabajadas en niveles anteriores por lo tanto se recomienda desarrollar:',
            caracteristicas: [
                {
                    showDescription: false,
                    icon: 'glasses',
                    title: 'Capacidad de transferencia a otros contextos',
                    description: 'En niveles anteriores los estudiantes habrán aprendido a desarrollar e implementar soluciones a problemáticas relativas a su campo específico de conocimiento, sin embargo ya para el nivel 3 se recomienda que se empiece a abrir el abanico de contextos diversos donde el estudiante podría desempeñarse de acuerdo con el respectivo perfil de egreso.'
                },
                {
                    showDescription: false,
                    icon: 'balance-scale',
                    title: 'Negociación',
                    description: 'El camino de la creatividad y de la innovación requiere que los estudiantes aprendan a negociar sus propuestas de solución no solo con los docentes sino con sus compañeros, y más adelante con sus clientes por lo que es indispensable que aprendan a negociar sin desmotivarse pero también que sepan cuándo desistir de una idea que aunque parezca prometedora en la realidad no se sustenta.'
                }
            ]
        }
    },
    introduccion: {
        descripcion: 'Representa los semestres quinto y sexto de aquellos programas que no están por niveles formativos.',
        lista: [
            'Formación profesional'
        ]
    }
}

export default {
    state
}