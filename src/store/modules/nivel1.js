const state = {
    competencias: {
        solucionDeProblemas: {
            intro: 'Para que en el nivel 1 se sienten las bases para que el estudiante, a lo largo de su trayectoria en educación superior, es recomendable que los docentes se enfoquen en desarrollar y evaluar: ',
            caracteristicas : [
                {   
                    showDescription: false,
                    icon: 'flask',
                    title:'Capacidad de investigación', 
                    description: 'Con el acceso a la  información al alcance de la mano que tienen los docentes deberán enseñar a buscar y seleccionar la información, que sea relevante y fidedigna, que atañe al problema específico. La habilidad asociada es la formulación  preguntas que guíen una buena recopilación de información.'
                },
                {
                    showDescription: false,
                    icon: 'chart-bar',
                    title:'Pensamiento analítico', 
                    description: 'Es importante desarrollar el pensamiento analítico, que el estudiante comprenda el problema a resolver y que sea capaz de separar cada uno de sus componentes para analizarlos a profundidad. Las habilidades asociadas serían la identificación de atributos y componentes, así como de las relaciones existentes entre ellos y los patrones que emergen del conjunto analizado. El análisis de un problema se podría ponderar con una evaluación más alta incluso que la solución porque el énfasis es el análisis en este nivel.'
                },
                {
                    showDescription: false,
                    icon: 'hiking',
                    title:'Pensamiento crítico', 
                    description: 'Se relaciona con la capacidad que tienen los estudiantes para cuestionar las cosas y buscar formas alternativas de solucionar los problemas. No obstante, más allá de que los estudiantes resuelvan problemáticas, se pretende que vayan adquiriendo juicio propio para evaluar sus propias tentativas de solución, en cierto sentido habrá que coadyuvar a que los estudiantes reflexionen sobre su aprendizaje y monitoreen su desempeño así como que detecten y corrijan errores.'
                },
                {
                    showDescription: false,
                    icon: 'object-group',
                    title:'Pensamiento sistémico', 
                    description: 'Éste permitirá que los estudiantes analicen los problemas como un conjunto de partes interrelacionadas en donde la modificación de un elemento del conjunto tendrá efectos diferentes en otros elementos por lo que se tendrán que valorar en conjunto las variables implicadas. '
                },
                {
                    showDescription: false,
                    icon: 'child',
                    title:'Trabajo individual', 
                    description: 'Si bien es ampliamente reconocido el trabajo en equipo como una forma de generar sinergias y lograr mejores resultados que trabajando de forma individual, lo cierto es que los estudiantes ingresas al nivel superior sin saber hacerlo. Uno de los principios del trabajo en equipo es el compromiso y la responsabilidad que deben tener los integrantes para el cumplimiento de las metas preestablecidas; sin embargo, el que un estudiante aprenda a trabajar en equipo tiene como precondición que sepa trabajar de manera individual. Por lo tanto, en el nivel formativo 1 se recomienda que se enseñe a los estudiantes a trabajar de manera individual.'
                }
            ]   
        },
        creatividad: {
            intro: 'Para que en el nivel 1 se sienten las bases para que el estudiante, a lo largo de su trayectoria en educación superior, es recomendable que los docentes se enfoquen en desarrollar y evaluar: ',
            caracteristicas: [
                {
                    showDescription: false,
                    title:'Autoestima', 
                    description: 'Para que se manifieste la creatividad en ca	da uno de los estudiantes será necesario desarrollar la confianza en sí mismos, el valor de enfrentar adversidades y apertura a diferentes puntos de vista.',
                    icon: 'grin-beam'
                },
                {
                    showDescription: false,
                    title:'Imaginación', 
                    description: 'La solución creativa de problemáticas requiere de estudiantes que sean capaces de representar mentalmente diferentes soluciones para salir de las respuestas tradicionales o de “libro”. Las mejores estrategias para fomentar la imaginación se conocen como estrategias de “pensamiento lateral”.',
                    icon: 'space-shuttle'
                }
            ]
        },
        innovacion: {
            intro: 'Una de las competencias del área de formación inicial es desarrollar el liderazgo en los estudiantes.',
            caracteristicas: [
                {
                    showDescription: false,
                    icon: 'bullhorn',
                    title:'Liderazgo', 
                    description: 'En los cimientos del liderazgo yace la organización, planeación,  control de riesgos, motivación y comunicación, entre otros; por lo que las unidades de aprendizaje del área de Formación Institucional deberán focalizar dichos elementos para irlos desarrollando ya que la innovación se refiere a la implementación de métodos,  procesos y/o sistemas nuevos lo que requiere que los estudiantes, adicionalmente a haber desarrollado la competencia de creatividad, sean unos líderes que guíen al equipo hasta la consecución de los objetivos.'
                }
            ]
        }
    },
    introduccion:{
        descripcion: 'Representa los semestres primero y segundo de aquellos programas que no están por niveles formativos. El nivel 1 representa el primer año de los estudiantes en la educación superior, por lo que los docentes forzosamente necesitan realizar un examen diagnóstico que les permita valorar realmente, más allá de lo que obtuvo el estudiante en su examen de ingreso, cuál es el desarrollo de las competencias que los estudiantes ya han adquirido en el nivel medio superior y cuáles aún no ha logrado desarrollar o bien se encuentran en estadios por debajo de lo esperado a su ingreso.',
        lista: [
            'Formación institucional',
            'Formación científica-básica',
            'Formación profesional (en algunos programas)'
        ]
    }
}

export default {
    state
}