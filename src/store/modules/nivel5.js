const state = {
    competencias: {
        solucionDeProblemas: {
            intro: 'En las carreras de 8 semestres el nivel cuatro será el último año escolar y en las de 9 o 10 semestres el nivel 5. Desde que los estudiantes empiezan nivel 4, para todas las carreras de ingeniería del IPN empezarán a cursar los créditos del área de formación terminal e integral por lo que los problemas a solucionar por ellos deberán ser, en la medida de lo posible, abordados de manera interdisciplinaria. Esto significa que se debe buscar que los estudiantes integren conocimientos adquiridos a lo largo de su trayectoria académica e incluso, tengan que aprender de manera autodidáctica nuevos conocimientos o actualizar los aprendidos. A este nivel, los docentes tendrán que valorar más las soluciones sean creativas e innovadoras focalizando la relación costo beneficio.',
            caracteristicas: []
        },
        creatividad: {
            intro: 'En estos niveles, los docentes deberán focalizar la evaluación de la creatividad en todas las soluciones a las problemáticas planteadas a los estudiantes; no solo que aporten ideas originales sino soluciones complejas que sean prácticas de ponerse en funcionamiento porque deberían de estar plasmadas de una manera formal con su respectivo análisis costo – beneficio de cada alternativa. Se requiere que los docentes que imparten unidades de aprendizaje asignen una ponderación más alta a soluciones alternativas que a soluciones, aunque efectivas, son estándar o de “libro”.',
            caracteristicas: []
        },
        innovacion: {
            intro: 'En estos niveles es aconsejable fortalecer las actitudes y valores relacionados con la innovación y el manejo del cambio. El análisis de riesgos se hace imprescindible y la implementación (por lo menos en papel) donde se describa con precisión los beneficios y los costos asociados.',
            caracteristicas: []
        }
    },
    introduccion: {
        descripcion: 'Representa los semestres noveno y décimo de aquellos programas que no están por niveles formativos. Para aquellos programas de 9 o 10 semestres, los estudiantes egresarán una vez cumplidos los requisitos específicos propios de cada carrera.',
        lista: [
            'Formación terminal y de integración',
            'Servicio Social'
        ]
    }
}

export default {
    state
}