const state = {
    competencias: {
        solucionDeProblemas: {
            intro: 'Para que en el nivel 2 se sienten las bases para que el estudiante, a lo largo de su trayectoria en educación superior, es recomendable que los docentes se enfoquen en desarrollar y evaluar: ',
            caracteristicas: [
                {
                    showDescription: false,
                    icon: 'compress-arrows-alt',
                    title: 'Capacidad de síntesis',
                    description: 'Si bien en el nivel ya se trabajó con los estudiantes del nivel formativo 1 el pensamiento analítico, a partir del nivel 2 se deberá ponderar más la capacidad de síntesis de todos los elementos analizados en dos o tres soluciones prácticas a los problemas. Las habilidades asociadas serían la capacidad de reestructurar los elementos analizados para proponer soluciones claras y efectivas.'
                },
                {
                    showDescription: false,
                    icon: 'users-cog',
                    title: 'Trabajo en equipo',
                    description: 'A partir de este nivel también se recomienda enseñar a los estudiantes a resolver problemáticas en equipo para lo cual se deberá coadyuvar a los estudiantes en la integración, planeación, organización y gestión de equipos y tareas. También se les debe enseñar técnicas de manejo de conflictos y negociación para que ellos solos vayan gestionando el desempeño de los equipos, mantener la dirección, hasta la consecución de las metas preestablecidas.'
                }
            ]
        },
        creatividad: {
            intro: 'Al igual que en el nivel anterior, para el nivel 2 lo recomendable es que los docentes sean flexibles y soliciten a los estudiantes adicional a una o dos soluciones eficientes a la problemática planteada, al menos una solución alternativa que salga del marco del contexto. Así, los docentes de este nivel deberán enfocarse a planteamientos altamente estructurados por parte de los estudiantes; adicionalmente, se deberá focalizar:',
            caracteristicas: [
                {
                    showDescription: false,
                    title: 'Pensamiento divergente',
                    description: 'Para que se manifieste la creatividad en cada uno de los estudiantes será necesario desarrollar la confianza en sí mismos, el valor de enfrentar adversidades y apertura a diferentes puntos de vista.',
                    icon: 'street-view'
                },
                {
                    showDescription: false,
                    title: 'Comunicación efectiva',
                    description: 'La imaginación debe estar acompañada de la capacidad de expresarlas de forma clara y concisa, tanto de forma oral como escrita. ',
                    icon: 'comments'
                },
                {
                    showDescription: false,
                    title: 'Trabajo en equipo',
                    description: 'Genera sinergia y es muy relevante que se les enseña a trabajar en equipos interdisciplinarios (que responden a soluciones más integrales). La organización, ',
                    icon: 'users'
                }
            ]
        },
        innovacion: {
            intro: 'A partir de este nivel, los estudiantes deberán necesitan aprender a emprender y a considerar qué o cómo se verá afectada la organización derivado de la introducción de innovaciones;  por lo tanto se recomienda desarrollar:',
            caracteristicas: [
            {
                showDescription: false,
                icon: 'globe',
                title: 'Espíritu emprendedor',
                description: 'Más allá de buscar que los estudiantes emprendan negocios para generar riqueza y acrecentar la calidad de vida, en esta investigación también se abarca el emprendimiento como la capacidad de proyectar planes (de vida y profesionales) y buscar con ahínco su realización con responsabilidad.'
            },
            {
                showDescription: false,
                icon: 'marker',
                title: 'Toma de decisiones',
                description: 'Que en su mayoría corresponderán a la complejidad porque están atravesadas por variables económicos, administrativas, políticas, estructurales, familiares, financieras, etc.'
            }
        ]
        }
    },
    introduccion: {
        descripcion: 'Representa los semestres tercero y cuarto de aquellos programas que no están por niveles formativos.',
        lista: [
            'Formación científica-básica',
            'Formación profesional (en algunos programas)'
        ]
    }
}

export default {
    state
}