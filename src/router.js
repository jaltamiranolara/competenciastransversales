import Vue from 'vue'
import Router from 'vue-router'
import App from './App'
import nivel from './components/niveles/nivel'
import introduccion from './components/introduccion'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', component: App,
      children: [
        {
          path: '',
          name: 'introduccion',
          component: introduccion,
        },
        {
          path: 'nivel/:id',
          name: 'nivel',
          component: nivel
        }
      ]
    }
  ]
})
